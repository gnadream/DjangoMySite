from django.db import models


class Article(models.Model):
    article_id = models.AutoField(primary_key=True)


class Book(models.Model):
    book_id = models.AutoField(primary_key=True)


class BookReview(Book, Article):
    page_count = models.IntegerField(default=0)


class BookA(models.Model):
    page_count = models.IntegerField()


class BookB(BookA):
    page_size = models.IntegerField()


class BookC(BookB):
    book_name = models.CharField(max_length=100)
