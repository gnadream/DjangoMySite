from django.db import models


class Person(models.Model):
    SHIRT_SIZES = (
        ('S', 'Small'),
        ('M', 'Medium'),
        ('L', 'Large')
    )
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200, default='x')
    second_name = models.IntegerField(blank=True, default=0)
    shirt_size = models.CharField(max_length=1, choices=SHIRT_SIZES, default='')
    id_card = models.CharField(max_length=100, unique=True)


class Musician(models.Model):
    # 字符串.最大长度50
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    # 字符串.最大长度100
    instrument = models.CharField(max_length=100)


class Album(models.Model):
    # 外键。models.CASCADE表示对应Musician的数据删除时，本表的数据会自动删除
    artist = models.ForeignKey(Musician, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    # 日期字段。不包含实践部分
    release_date = models.DateField()
    # 数字
    num_stars = models.IntegerField()


class PersonDetail(models.Model):
    user_name = models.CharField(max_length=100, primary_key=True)
    height = models.IntegerField(default=0)

