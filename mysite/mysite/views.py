from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect
from django.http import HttpResponseGone, JsonResponse, FileResponse
from django.utils import timezone
import datetime
from django.conf import settings
import json
import os

def default_index(request):
    response = HttpResponse("the boy is touch himself face")
    response.set_cookie("username", "kamihati", max_age=60, path="/")
    response.write("<br />")
    response.write("oh no!!!!!!!!!!!!!!")

    response.write("<h1>so xxxxxx</h1>")
    response['boy_age'] = 18
    response['boy_height'] = '1米五'

    del response['boy_age']
    print("response.streaming=", response.streaming)

    response.closed = True
    print('response.closed=', response.closed)
    response.write("<h2>风生之谷，客从山来。</h2>")

    response = HttpResponse("道不虚行只在人", content_type="text/html")
    print('response.status_code=', response.status_code)
    print('response.reason_parse=', response.reason_phrase)

    response['boy_name'] = 'young people'
    response.__setitem__('boy_sex', 'middle')

    print('response.__getitem__=', response.__getitem__('boy_sex'))
    print(response['boy_sex'])

    print('xxxxxxxxxxxx')
    print(response.setdefault('boy_sex3', 'man'))

    print('set cookie')
    print('now time=', timezone.now())
    """
    response.set_cookie("login_user_id", 'kamihati', max_age=10,
                        expires=timezone.now() + datetime.timedelta(days=1),
                        domain="127.0.0.1", path="/")
    """
    print('read cookie')
    # print(response.cookies['login_user_id'])
    login_user_id = request.COOKIES.get('login_user_id', "")
    response.write('cookie login_user_id =' + login_user_id)
    return response


def download_file(request, file_type):
    if file_type == 1:
        file_content = "奥德赛发送到发的说法是的发送到发的说法"
        response = HttpResponse(file_content, content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment;filename="haha.txt"'
        return response
    else:
        return HttpResponse("未处理的文件类型")


def write_value(request):
    response = HttpResponse()
    response.write("write signed cookie")
    response.set_signed_cookie("user_buy_count", 3,
                               salt=settings.SIGNED_COOKIES_SALT,
                               path="/", domain='127.0.0.1')
    return response


def read_value(request):

    content = "username=" + request.get_signed_cookie(
        "user_buy_count", salt=settings.SIGNED_COOKIES_SALT)
    response = HttpResponse(content)

    # response.delete_cookie("user_buy_c2ount", path="/", domain="127.0.0.1")

    return response


def show_json(request):
    d = dict(a=1, b='我', c=['a', 'b', 'd'])
    response = JsonResponse(d)
    return response


def download_img(request):

    file_path = os.path.join(settings.BASE_DIR,
                             "polls/static/polls/images/background.jpg")
    file_obj = open(file_path, "rb")
    response = FileResponse(file_obj, as_attachment=True, filename="01.jpg")
    return response


def download_iso(request):
    file_path = os.path.join(settings.BASE_DIR, "polls/static/win7.iso")
    file_obj = open(file_path, "rb")
    response = FileResponse(file_obj, as_attachment=True)
    return response
