"""
Django settings for mysite project.

Generated by 'django-admin startproject' using Django 2.1.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# 用户上传的文件保存的目录
USER_UPLOAD_DIR = os.path.join(BASE_DIR, "user_upload")


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '7dnb0kn5%2^%br7suhf95mda3fb$(@%ard1zio@n$_+tma@(y5'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',

    'polls.apps.PollsConfig',
    'teach.apps.TeachConfig',
    'forum.apps.ForumConfig',
    'users.apps.UsersConfig',
    'items.apps.ItemsConfig',
    'blog.apps.BlogConfig',
    'parts.apps.PartsConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
]

ROOT_URLCONF = 'mysite.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.jinja2.Jinja2',
        'DIRS': [
        ],
        'APP_DIRS': True,
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # os.path.join(BASE_DIR, "polls/templates"),
            # os.path.join(BASE_DIR, "parts/templates"),
            os.path.join(BASE_DIR, 'templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },

]

WSGI_APPLICATION = 'mysite.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'mysite',
        'HOST': '127.0.0.1',
        'PORT': 3306,
        "PASSWORD": '123456',
        'USER': 'root',
        'TEST': {
            'NAME': 'test_db_mysite',
            'CHARSET': 'utf8',
            'COLLATION': 'utf8_general_ci'
        },

    },
    'readonly_db': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'mysite_readonly',
        'HOST': '127.0.0.1',
        'PORT': 3306,
        "PASSWORD": '123456',
        'USER': 'root',
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

# TIME_ZONE = 'UTC'
TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'

# 用于保存和读取signed_cookie
SIGNED_COOKIES_SALT = "~`@#$@#$&_23&^9823&%$#"

STATICFILES_DIRS = [
    ("downloads", os.path.join(BASE_DIR, "download_file")),
    (os.path.join(BASE_DIR, "download_file"))
]

# media相关配置
# media文件根目录,相对于根目录的相对路径
MEDIA_ROOT = "user_upload/"
# 访问MEDIA_ROOT目录下资源使用的url
MEDIA_URL = "/media/"

# 发送邮件时若未指定from_email则使用此配置
DEFAULT_FROM_EMAIL = "127777@qq.com"

# 若未指定此配置则系统默认为/account/login/
LOGIN_URL = '/users/auth_login/'

# 邮件配置
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = "smtp.qq.com"
EMAIL_PORT = 465
EMAIL_HOST_USER = '1593508565@qq.com'
EMAIL_FROM = EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = "obqegiyjgbjvgged"
# 邮箱连接设置为使用ssl时。需要设置此选项为True,例如qq邮箱默认设置即为此
EMAIL_USE_SSL = True

# 用户头像url
PORTRAIT_DEFAULT_URL = STATIC_URL + "images/default_portrait.jpg"
