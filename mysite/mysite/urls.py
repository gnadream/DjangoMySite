"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from . import views as mysite_views


urlpatterns = [
    path("", mysite_views.default_index, name='default_index'),
    path('write/', mysite_views.write_value, name='write_value'),
    path('read/', mysite_views.read_value, name='read_value'),
    path('show_json/', mysite_views.show_json, name='show_json'),
    path('download_img/', mysite_views.download_img, name='download_img'),
    path('download_iso/', mysite_views.download_iso, name='download_iso'),
    path('download/<int:file_type>/', mysite_views.download_file, name='download_file'),
    path("polls/", include("polls.urls")),
    path("parts/", include("parts.urls")),
    path('users/', include("users.urls")),
    path('admin/', admin.site.urls),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
