from django.contrib import admin
from .models import UserDetail, Bar, LoginUser

# Register your models here.
admin.site.register(UserDetail)
admin.site.register(Bar)
admin.site.register(LoginUser)