from django.urls import path
from . import views, auth_views

app_name = 'users'

urlpatterns = [
    path("register/", views.user_register, name='user_register'),
    path("register_success/", views.user_register_success, name='register_success'),
    path("register_fail/", views.user_register_fail, name="register_fail"),
    path("login/", views.user_login, name='user_login'),
    path("index/", views.user_index, name='user_index'),
    path("logout/", views.user_logout, name='logout'),
]

# 专用于auth_views.py的url配置
urlpatterns += [
    path('auth_register/', auth_views.auth_register, name='auth_register'),
    path('auth_login/', auth_views.user_login, name='auth_login'),
    path('login_success/', auth_views.user_login_success, name='login_success'),
    path('auth_logout/', auth_views.auth_logout, name='auth_logout'),
    path('auth_info/', auth_views.auth_info, name='auth_info'),
    path('auth_detail/', auth_views.auth_detail, name='auth_detail'),

    path('ajax_login/', auth_views.user_login_static, name='user_login_static'),
    path('ajax_auth_login/', auth_views.ajax_user_login, name='ajax_auth_login')
]