from django.db import models


class Commet(models.Model):
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=5000)
    parent_comment = models.ForeignKey('self', on_delete=models.CASCADE, null=True)


class Author(models.Model):
    real_name = models.CharField(max_length=100)


class Book(models.Model):
    author = models.ManyToManyField(Author)
    book_name = models.CharField(max_length=50, default='')

