from django.shortcuts import render
from django.utils import timezone
import datetime


def humanize_show(request):
    context = dict(date_now=timezone.now(),
                   date_tomorrow=timezone.now() + datetime.timedelta(days=1),
                   date_yesterday=timezone.now() - datetime.timedelta(days=1),
                   date_oldday=timezone.now() - datetime.timedelta(days=2))
    return render(request, "parts/humanize_view.html", context)

