from django.shortcuts import render
from django.utils import timezone


def show_filter(request):
    user_list = [
        {"name": 'zed', 'age': 19},
        {"name": "amy", "age": 22},
        {"name": "joe", "age": 31},
    ]

    datas = dict(name='两仪式',
                 v1=None,
                 v2=0,
                 v3="",
                 age=18,
                 height=150,
                 login_time=timezone.now(),
                 num_list=list(range(10)),
                 v="Joel is a slug",
                 v4="谢瑞阳 在 站着",
                 v5='<p>我们 是 社会主义 接班人</p>',
                 user_list=user_list,
                 polls_index="<a href='/polls/'>投票首页</a>",
                 login_user=dict(username='kamihati', nickname='白衣沽酒绮罗生'))

    return render(request, "parts/show_filter.html", datas)
