from django import template
register = template.Library()


def change_str(value):
    if type(value) != str:
        return value
    return "[6折,双11当天3折]" + value


# 使用参数并且指定参数可不传的过滤器
@register.filter(name="change_str2")
def change_str(value, args=None):
    if type(value) != str:
        return value

    result = "[6折,双11当天3折]" + value
    if args == 1:
        result = '【毛折扣啊。赔死了】' + value
    elif args == 2:
        result = "【不在淘宝卖了，清仓处理。1折起】" + value
    return result


# 注册方式2，装饰器,默认不传参数可以使用函数名作为过滤器名称
@register.filter
def bold_str(value):
    return "<b>%s</b>" % value


# 注册方式1：把change_str函数注册为过滤器
register.filter("change_str", change_str)
# register.filter(name="change_str", filter_func=change_str)


# 最简单的标签注册方式，默认使用函数名作为标签名，
# 标签参数规则与python一般函数一致
@register.simple_tag
def replace_str(str_base, re_str, p_str=""):
    return str_base.replace(re_str, p_str)


# inclusion_tag
# 结合模板文件进行渲染并把渲染结果输出到调用标签的位置
@register.inclusion_tag("parts/tags_show_poll_choice.html")
def show_poll_choices(question):
    choices = question.choice_set.all()
    return {"choices": choices}
