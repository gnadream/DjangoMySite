
from django.urls import path, reverse
from . import views, pdf_view, filter_views, humanize_views as hm_views
from . import filter_custom, tags_custom_views

app_name = "parts"

urlpatterns = [
    path('', views.index, name='index'),
    path("upload_file/", views.user_upload_file, name='user_upload_file'),
    path("upload_success/", views.upload_success, name="upload_success"),
    path("upload_fail/", views.upload_fail, name="upload_fail"),

    path("make_csv/", views.make_csv, name='make_csv'),
    path("make_large_csv/", views.some_streaming_csv_view, name="make_large_csv"),
    path("make_csv_by_t/", views.make_csv_by_template, name="make_csv_by_template")
]

# pdf_view相关url配置
urlpatterns += [
    path('pdf_io/', pdf_view.pdf_by_io, name='pdf_by_io'),
    path('pdf_res/', pdf_view.pdf_by_response, name='pdf_by_response'),
]

# filter_views.py相关url配置
urlpatterns += [
    path('show_filter/', filter_views.show_filter, name='show_filter')
]


# filter_views.py相关url配置
urlpatterns += [
    path('show_humanize/', hm_views.humanize_show, name='show_filter')
]

# filter_custom.py 相关url配置
urlpatterns += [
    path("filter_custom/", filter_custom.filter_custom_show, name='filter_custom')
]

# tags_custom.py相关url配置
urlpatterns += [
    path("tags_custom/", tags_custom_views.show_tags_custom, name='show_tags_custom'),
]
