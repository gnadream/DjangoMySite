from django.shortcuts import render
from polls.models import Question


def show_tags_custom(request):
    context = {
        "a": 1,
        "b": [1, 2, 3],
        "c": dict(a=1, b=2, c=3),
        "question": Question.objects.get(pk=1)
    }
    return render(request, "parts/tags_custom.html", context)
