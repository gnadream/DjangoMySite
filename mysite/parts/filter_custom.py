from django.shortcuts import render


def filter_custom_show(request):
    context = dict()
    context['commodity_list'] = [
        "黑色长款修身狐狸毛羽绒服",
        "灰色加绒热裤",
        "先祖热裤"
    ]
    context["num_list"] = list(range(5))
    return render(request, "parts/filter_custom.html", context)
