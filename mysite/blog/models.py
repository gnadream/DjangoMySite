from django.db import models
from django.utils import timezone


# 自定义manager需要继承models.Manager
class BlogManager(models.Manager):

    def create_blog(self, name, tagline):
        blog = self.create(name=name, tagline=tagline)

        # 做一些想做的事
        blog.tagline += 'xxxxx'
        blog.save()
        return blog


class Blog(models.Model):
    name = models.CharField(max_length=100)
    tagline = models.TextField()
    create_time = models.DateTimeField(default=timezone.now)

    objects = BlogManager()

    def __str__(self):
        """
        当实例被转为str的时候显示什么内容
        :return:
        """
        return self.name


class Author(models.Model):
    AuthorSex = (
        ('M', 'Man'),
        ('W', 'Women')
    )

    name = models.CharField(max_length=200, unique=True)
    email = models.EmailField()
    test_num = models.IntegerField(null=True)
    sex = models.CharField(max_length=1, choices=AuthorSex, default='W')

    def __str__(self):
        return self.name


class Entry(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    headline = models.CharField(max_length=255)
    body_text = models.TextField()
    pub_date = models.DateField()
    mod_date = models.DateField()
    authors = models.ManyToManyField(Author)
    n_comments = models.IntegerField()
    n_pingbacks = models.IntegerField()
    rating = models.IntegerField()
    test_datetime = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.headline
