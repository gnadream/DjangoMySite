from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from django.utils import timezone


def jinja2_by_template(request):
    return render(request, "polls/index.html")


def index_1(request):
    template = loader.get_template("polls/t_index.html")
    datas = dict(page_name="index_1")
    return HttpResponse(template.render(datas, request))


def index_2(request):
    datas = dict(page_name="index_2",
                 list_0=['1111', '2222'],
                 login_time=timezone.now(),
                 user_info=dict(userid='kamihait',
                                height=183,
                                weight=79,
                                nickname='白衣沽酒绮罗生'),
                 num_list=list(range(10)))
    response = render(request, "polls/t_index.html", datas)
    return response


def show_base(request):
    datas = dict()

    return render(request, "polls/base.html", datas)


def use_base(request, num):
    num_list = list(range(10))
    num_list.append(9)

    data_list = [(1, 1), (2, 2,), (3, 3), (3, 3), (3, 4)]
    datas = dict(num_list=num_list,
                 empty_list=[1],
                 data_list=data_list,
                 dict_0=dict(name='云天河', father="云天青"),
                 is_black=1,
                 is_red=2,
                 is_yellow=0)
    return render(request, "polls/use_base_%s.html" % num, datas)
