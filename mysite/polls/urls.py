
from django.urls import path, re_path, register_converter
# from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views, urls_converters, template_view
from . import form_views

register_converter(urls_converters.FourDigitYearConverter, 'yyyy')

# 定义app_name可用于动态调用url path的name,
# 但是定义之后就不能单独使用url path的name获取url
app_name = "polls"

urlpatterns = [
    path('', views.index, name='index'),
    #
    # # 使用参数.格式1
    # path('bio/<username>', views.bio, name='bio'),
    # # 限制参数类型
    # path('num/<int:num>', views.num, name='num'),
    #
    # # 使用正则定义url参数
    # re_path('bio2/(?P<username>\w+)/$', views.bio2, name='bio2'),
    #
    # # 例： /polls/5/
    path("<int:question_id>/", views.detail, name='detail'),
    # 例： /polls/5/result/
    path("<int:question_id>/results/", views.results, name='results'),

    path('', views.IndexView.as_view(), name='index'),
    # path('<int:pk>', views.DetailView.as_view(), name='detail'),
    # path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # 例: /polls/5/vote/
    path("<int:question_id>/vote/", views.vote, name='vote'),

    path("<yyyy:year>/question/", views.show_year_question, name='year_question'),
    re_path(r"^(?P<year>[0-9]{4})/year_question/$", views.show_year_question, name='year_question_two'),
    path("question_list/", views.quesiton_list, name='quesiton_list'),
]


urlpatterns += [
    path('tview/', template_view.jinja2_by_template, name='template_view'),
    path('tview1/', template_view.index_1, name='template_index_1'),
    path('tview2/', template_view.index_2, name='template_index_2'),
    path('show_base/', template_view.show_base, name='show_base'),
    path('use_base/<int:num>/', template_view.use_base, name='use_base'),
]


# form_views.py相关配置
urlpatterns += [
    path('form_show/', form_views.form_show, name='form_show'),
    path('login_success/', form_views.form_login_success, name='login_success'),
    path("form_field/", form_views.form_field_show, name='form_field_show'),
]
