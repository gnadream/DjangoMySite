from django.contrib import admin

from .models import Question, Choice


class ChoiceInline(admin.TabularInline):
    model = Choice
    # 默认显示多少组表单
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    # fields 的字段顺序。映像对model数据的修改页面的表单顺序
    # fields = ['pub_date', 'question_text']

    # 控制model数据管理页面的字段分组，
    # 分组内可控制分组标题以及分组的字段列表等
    # 分组标题设为None。则此分组不显示标题样式
    fieldsets = [
        (None, {'fields': ['question_text']}),
        ('日期部分', {'fields': ['pub_date']})
    ]
    inlines = [ChoiceInline]

    # 控制列表页面都显示什么字段
    list_display = ['question_text', 'pub_date', 'was_published_recently']

    # 控制列表filter的字段
    list_filter = ['pub_date']

    # 控制模糊搜索的字段范围
    search_fields = ['question_text__startswith']

    # 控制每页显示数据的数量并以此分页
    list_per_page = 3


admin.site.register(Question, QuestionAdmin)
