from django.db import models
from django.utils import timezone
import datetime


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('发布时间')

    def __str__(self):
        """
        把question_text的值当做实例转为字符串的结果
        :return:
        """
        return self.question_text

    def was_published_recently(self):
        """
        Question的发布时间是否为1天之内，是则返回True,否则返回False
        发布时间若晚于当前时间。也会返回False
        :return:
        """
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    # 设定点击后台列表中的方法时的排序字段
    was_published_recently.admin_order_field = 'pub_date'
    # 控制列表中字段标题列的显示内容
    was_published_recently.short_description = '是否最近发布?'
    # 控制bool型的值是否使用django的bool样式
    was_published_recently.boolean = True


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
