from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse
from .forms import LoginForm, TestForm


def form_show(request):

    context = dict()
    form = LoginForm()
    if request.method == "POST":
        form = LoginForm(request.POST)
        # is_valid()方法用于验证提交的数据是否符合Form类的字段定义
        if form.is_valid():
            return HttpResponseRedirect(reverse("polls:login_success"))
    context['form'] = form
    return render(request, "form/base.html", context)


def form_login_success(request):
    return HttpResponse("login success, ok.")


def form_field_show(request):
    """
    演示form各种字段表现
    :param request:
    :return:
    """
    context = dict()
    # auto_id=True生成的页面html元素会自动生成id,False则不会
    form = TestForm(auto_id=False)
    if request.method == "POST":
        print(request.POST)
        form = TestForm(request.POST)
        print(form.data)
        # is_valid()方法用于验证提交的数据是否符合Form类的字段定义
        if form.is_valid():
            print(form.cleaned_data)
            return HttpResponseRedirect(reverse("polls:login_success"))
    context['form'] = form
    return render(request, "form/field_show.html", context)
