from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=20, required=True, min_length=6,
                               label="账号", help_text="账号为6-20位长的字符串")
    password = forms.CharField(max_length=20, required=True, min_length=6,
                               label="密码", widget=forms.PasswordInput(),
                               )


class TestForm(forms.Form):
    is_true = forms.BooleanField(required=False)
    username = forms.CharField(strip=True, empty_value='xxx')
    password = forms.CharField(required=False, widget=forms.PasswordInput())
    hid_2 = forms.CharField(required=False, widget=forms.HiddenInput())
    age = forms.IntegerField(required=False, max_value=100, min_value=6)
    hid_1 = forms.CharField(required=False, widget=forms.HiddenInput())
    type_1 = forms.ChoiceField(required=False, choices=(("", "请选择"), (1, "一"), (2, "二"), (3, "三")))
    email = forms.EmailField(required=False, )
    height = forms.IntegerField(required=False, )
    birthday = forms.DateField(required=False, )
    birthday_full = forms.DateTimeField(required=False, )
    birthday_time = forms.TimeField(required=False, )
    detail = forms.CharField(required=False, widget=forms.Textarea(attrs=dict(rows=5, cols=30)))
    # hobby_one = forms.CharField(widget=forms.CheckboxInput())
    hobby = forms.CharField(required=False, widget=forms.CheckboxSelectMultiple(choices=((1, "吃"), (2, "喝"), (3, "玩"), (4, "学习"))))
    sex = forms.CharField(required=False, widget=forms.Select(choices=(("", "请选择"), ('1', "男"), (0, "女"))))
    is_rich = forms.NullBooleanField(required=False, widget=forms.NullBooleanSelect())
    hobby_2 = forms.CharField(required=False, widget=forms.SelectMultiple(choices=((1, "吃"), (2, "喝"), (3, "玩"), (4, "学习"))))
    food_hobby = forms.CharField(required=False, widget=forms.RadioSelect(choices=((1, "吃"), (2, "喝"), (3, "玩"), (4, "学习"))))

    portrait = forms.FileField(required=False, allow_empty_file=True)

    join_time = forms.CharField(widget=forms.SelectDateWidget(
        years=(2007, 2009),
        months={"": "请选择", "1": "一月", "2": "二月"},
        empty_label=("9", "10", "11")
    ))


