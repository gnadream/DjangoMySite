from django.test import TestCase

import datetime
from django.utils import timezone
import time
from django.urls import reverse
from .models import Question


def create_question(question_text, days):

    publish_time = timezone.now() + datetime.timedelta(days=days)
    # 直接使用传入的数据创建数据库的数据,创建成功则返回Question的实例
    return Question.objects.create(question_text=question_text,
                                   pub_date=publish_time)


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        future_time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=future_time)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        old_time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=old_time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        recent_time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=recent_time)
        self.assertIs(recent_question.was_published_recently(), True)


class QuestionIndexViewTests(TestCase):

    def test_no_questions(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        # 判断返回结果response中是否包含指定的字符串
        self.assertContains(response, '没有待投票的问题')
        # 判断两个queryset的数据是否相同
        self.assertQuerysetEqual(response.context['lasted_question_list'], [])

    def test_past_question(self):
        create_question('过去发布的question', -30)
        # create_question('past question', -30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['lasted_question_list'],
                                 ['<Question: 过去发布的question>'])

    def test_future_question(self):
        # create_question('未来发布的question', 30)
        create_question("future question", 30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, '没有待投票的问题')
        self.assertQuerysetEqual(response.context["lasted_question_list"],
                                 [])

    def test_future_question_and_past_question(self):
        # create_question('过去发布的question', -30)
        create_question('past question', -30)
        # create_question('未来发布的question', 30)
        create_question('future question', 30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['lasted_question_list'],
                                 ['<Question: past question>'])

    def test_two_past_question(self):
        # create_question('30天前的数据', -30)
        # create_question('5天前的数据', -5)
        create_question('past question 30 days', -30)
        create_question('past question 5 days', -5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['lasted_question_list'],
            ['<Question: past question 5 days>', '<Question: past question 30 days>']
        )


class QuestionDetailViewTests(TestCase):
    """
    测试点击问题后的详情页面detail相关的view
    """

    def test_future_question(self):
        future_question = create_question(question_text='Future question', days=5)
        url = reverse('polls:detail', args=(future_question.id, ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_question(question_text='Past Question', days=-5)
        url = reverse('polls:detail', args=(past_question.id, ))
        response = self.client.get(url)
        # 断言页面输出结果里包含问题的question_text
        self.assertContains(response, past_question.question_text)

