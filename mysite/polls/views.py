from django.shortcuts import redirect, render, get_object_or_404, get_list_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect, HttpResponseNotFound
from django.urls import reverse
from .models import Question, Choice
from django.views import generic
from django.utils import timezone
import json
from django.views.decorators.http import require_http_methods


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'lasted_question_list'

    def get_queryset(self):
        # pub_date__lte=timezone.now() 是判断pub_date小于等于当前时间
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by("-pub_date")[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def index(request):
    # polls的默认页面。会显示待投票的问题的列表.
    lastest_question_list = Question.objects.order_by("-pub_date")[:5]

    # 较为简单的使用模板文件的方式
    context = {
        "lasted_question_list": lastest_question_list
    }
    response = render(request, 'polls/index.html', context, using='django')
    return response


def bio(request, username):
    return HttpResponse("Hello, %s" % username)


def num(request, num):
    return HttpResponse("number is %s" % num)


def bio2(request, username):
    return HttpResponse("bio2's username=%s" % username)


def detail(request, question_id):
    print("this is detail page")
    if request.session.get("loginuser"):
        print('username=', request.session['loginuser'])
    else:
        print("未登录")

    print("site=", request.site)
    # question = get_object_or_404(Question, pk=question_id)
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("您查找的这个问题并不存在。请确认您传入的问题编号是否正确")
    return render(request, "polls/detail.html", {"question": question})


def results(request, question_id):
    print(request.META['QUERY_STRING'])
    print(request.META['SERVER_NAME'])
    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/results.html", {'question': question})


def vote(request, question_id):
    print("xxxxxxxxxxxxxx")

    print(request.POST)

    for k in request.POST:
        print("%s=%s" % (k, request.POST[k]))
    print("request.FILES")
    print(request.FILES)

    print("request.META=")
    print(request.META)
    for k, v in request.META.items():
        print('%s=%s' % (k, v))
    # get_object_or_404 抛出的404不能自定义提示内容。
    question = get_object_or_404(Question, pk=question_id)
    try:
        # request.POST可以获取到所有表单提交的数据
        if request.method == "POST":
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, "polls/detail.html", {
            'question': question,
            'error_message': "您投票的选项不存在或不正确"
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # HttpResponseRedirect可以跳转到指定地址
        # reverse方法可以获取指定url name的地址
        return HttpResponseRedirect(reverse('polls:results', args=(question.id, )))


def show_year_question(request, year):
    questions = Question.objects.filter(pub_date__year=year)
    return render(request, "polls/year_question.html",
                  {'question_year': questions})


def quesiton_list(request):
    for k, v in request.GET.items():
        print('%s=%s' % (k, request.GET.get(k)))

    print("POST")
    print(request.POST)


    datas = [dict(id=obj.id,
                  question_text=obj.question_text,
                  pub_date=obj.pub_date.strftime('%Y-%m-%d %H:%M:%S'))
             for obj in Question.objects.filter(pub_date__year=2017)]

    result = json.dumps(datas)
    # 下载文件所需的content_type设置
    # response = HttpResponse(result, content_type="application/octet-stream")
    # 注意：绝大多数情况下不要设置为301.否则用户浏览器可能会缓存此次设置。
    # 导致用户得不到服务器最新的修改
    response = HttpResponse(result)
    # return response
    # return redirect("/polls/")
    # question = get_object_or_404(Question, id=1)
    # question_list = get_list_or_404(Question, id__lt=0)
    return response
