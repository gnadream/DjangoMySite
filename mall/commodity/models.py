from django.db import models
from django.utils import timezone
from users.models import LoginUser

class CommoditySort(models.Model):
    sort_name = models.CharField(max_length=50)
    sort_shortcut = models.CharField(max_length=20)

    def __str__(self):
        return self.sort_shortcut


class Commodity(models.Model):
    commodity_no = models.CharField(max_length=100, unique=True)
    commodity_name = models.CharField(max_length=200)
    single_price = models.DecimalField(max_digits=10, decimal_places=2)
    commodity_sort = models.ForeignKey(CommoditySort, on_delete=models.CASCADE)
    stock = models.IntegerField(default=0)
    publish_date = models.DateTimeField(default=timezone.now)
    modify_date = models.DateTimeField(default=timezone.now)
    loginuser = models.ForeignKey(LoginUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.commodity_name


class CommodityModifyLog(models.Model):
    commodity = models.ForeignKey(Commodity, on_delete=models.CASCADE)
    modify_time = models.DateTimeField(default=timezone.now)
    modify_detail = models.TextField(default='')

