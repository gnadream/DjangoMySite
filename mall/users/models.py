from django.db import models
from django.utils import timezone


class LoginUser(models.Model):
    username = models.CharField(max_length=20, unique=True)
    password = models.CharField(max_length=100)
    question = models.CharField(max_length=100, default='')
    answer = models.CharField(max_length=100, default='')
    real_name = models.CharField(max_length=20, default='')
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    reg_time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.username



